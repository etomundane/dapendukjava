package com.etomundane.mobile.dapenduk.ui.login;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.util.android.SystemHelper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

public class LoginActivity extends DaggerAppCompatActivity implements LoginMVP.View {

    @Inject
    LoginPresenter<LoginMVP.View, LoginMVP.Mediator> presenter;

    @BindView(R.id.activity_login)
    ViewGroup rootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    View progressBar;

    @BindView(R.id.cb_remember)
    AppCompatCheckBox cbRemember;

    @BindView(R.id.et_username)
    EditText etUsername;


    @BindView(R.id.et_password)
    EditText etPassword;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar.getNavigationIcon() != null) {
                toolbar.getNavigationIcon()
                        .setColorFilter(ContextCompat.getColor(this, android.R.color.black), PorterDuff.Mode.MULTIPLY);
            }
        }
        presenter.attachView(this);
        presenter.isRememberPassword();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRememberPassword(boolean remember) {
        if (remember) {
            presenter.getRememberedPassword();
        }
    }

    @Override
    public void onPasswordRemembered(String username, String password) {
        cbRemember.setChecked(true);
        etUsername.setText(username);
        etPassword.setText(password);
    }

    @Override
    public void onLoginSuccess() {
        toast(getString(R.string.alert_login_success));
        finish();
    }

    @Override
    public void onUserNotExisted() {
        showSimpleSnackBar(getString(R.string.alert_user_not_exists));
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(String message) {
        showSimpleSnackBar(message);
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showSimpleSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.bt_login)
    void onClickLogin() {
        SystemHelper.hideSoftKeyboard(this, etUsername);
        setEditTextError(etUsername, false);
        setEditTextError(etPassword, false);

        String username = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        boolean remember = cbRemember.isChecked();
        if (username.isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_username_required));
            setEditTextError(etUsername, true);
        } else if (password.isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_password_required));
            setEditTextError(etPassword, true);
        } else {
            presenter.onSubmitLogin(username, password, remember);
        }

    }

    private void setEditTextError(View editText, boolean isError) {
        editText.setBackground(isError ?
                ContextCompat.getDrawable(this, R.drawable.background_my_edit_text_error) :
                ContextCompat.getDrawable(this, R.drawable.background_my_edit_text)
        );
    }
}
