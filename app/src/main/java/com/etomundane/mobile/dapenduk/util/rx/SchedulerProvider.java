package com.etomundane.mobile.dapenduk.util.rx;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Date Created: 2018-07-07 12:05:59
 * Author: Eto Mundane
 */
public class SchedulerProvider implements AppScheduler {

    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public <T> ObservableTransformer<T, T> ioObservableScheduler() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> SingleTransformer<T, T> ioSingleScheduler() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> FlowableTransformer<T, T> ioFlowableScheduler() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> MaybeTransformer<T, T> ioMaybeScheduler() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public CompletableTransformer ioCompletableScheduler() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> ObservableTransformer<T, T> computationObservableScheduler() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public <T> SingleTransformer<T, T> computationSingleScheduler() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public <T> FlowableTransformer<T, T> computationFlowableScheduler() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public <T> MaybeTransformer<T, T> computationMaybeScheduler() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public CompletableTransformer computationCompletableScheduler() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }
}
