package com.etomundane.mobile.dapenduk.ui.common;

import android.support.design.widget.AppBarLayout;

/**
 * Date Created: 2018-07-08 23:28:15
 * Author: Eto Mundane
 */
public class AppBarScrollWatcher implements AppBarLayout.OnOffsetChangedListener {
    private int scrollRange = -1;
    private OffsetListener listener;

    public AppBarScrollWatcher(OffsetListener listener) {
        this.listener = listener;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (scrollRange == -1) {
            scrollRange = appBarLayout.getTotalScrollRange();
        }

        int appbarHeight = scrollRange + verticalOffset;
        float alpha = (float) appbarHeight / scrollRange;
        int argbZeroOnExpanded = (int) Math.abs((alpha * 255) - 255);
        int argbZeroOnCollapsed = (int) Math.abs(alpha * 255);
        listener.onAppBarExpanding(argbZeroOnExpanded == 0, argbZeroOnCollapsed == 0, argbZeroOnExpanded, argbZeroOnCollapsed);
    }

    public interface OffsetListener {
        void onAppBarExpanding(boolean expanded, boolean collapsed, int argbZeroOnExpanded, int argbZeroOnCollapsed);
    }
}