package com.etomundane.mobile.dapenduk.data.repository.user;

import com.etomundane.mobile.dapenduk.data.local.database.user.User;
import com.etomundane.mobile.dapenduk.data.local.database.user.UserDao;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-06 02:09:23
 * Author: Eto Mundane
 */
@Singleton
public class LocalUserRepository implements UserRepository {
    private UserDao userDao;

    @Inject
    public LocalUserRepository(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Observable<Boolean> isAdminEmpty() {
        return Observable.fromCallable(() -> userDao.getAllAdmin().size() <= 0);
    }

    @Override
    public Observable<Long> insertAdminUser(User userAdmin) {
        return Observable.fromCallable(() -> userDao.insert(userAdmin));
    }

    @Override
    public Single<User> getAdminUser(long uid) {
        return Single.fromCallable(() -> userDao.getUser(uid));
    }

    @Override
    public Single<User> getAdminUser(String username, String password) {
        return Single.fromCallable(() -> userDao.getUser(username, password));
    }
}
