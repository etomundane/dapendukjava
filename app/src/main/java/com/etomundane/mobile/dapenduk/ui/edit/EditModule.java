package com.etomundane.mobile.dapenduk.ui.edit;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * Date Created: 2018-07-09 01:06:08
 * Author: Eto Mundane
 */
@Module
public abstract class EditModule {
    @ActivityScope
    @Binds
    abstract EditMVP.Mediator editMediator(@ActivityScope EditMediator mediator);

    @ActivityScope
    @Binds
    abstract EditMVP.Presenter editPresenter(@ActivityScope EditPresenter<EditMVP.View, EditMVP.Mediator> presenter);

}
