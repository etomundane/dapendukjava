package com.etomundane.mobile.dapenduk.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.ui.create.CreateActivity;
import com.etomundane.mobile.dapenduk.ui.detail.DetailActivity;
import com.etomundane.mobile.dapenduk.ui.edit.EditActivity;
import com.etomundane.mobile.dapenduk.ui.login.LoginActivity;
import com.etomundane.mobile.dapenduk.util.android.AppHelper;
import com.etomundane.mobile.dapenduk.util.android.SystemHelper;
import com.etomundane.mobile.dapenduk.util.android.ViewHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

@ActivityScope
public class HomeActivity extends DaggerAppCompatActivity implements HomeMVP.View, ResidentListCallback {

    @Inject
    HomePresenter<HomeMVP.View, HomeMVP.Mediator> presenter;

    @Inject
    ResidentListAdapter adapter;

    @BindView(R.id.activity_home)
    ViewGroup rootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.search_bar)
    View searchBar;

    @BindView(R.id.search_clear)
    View searchClear;

    @BindView(R.id.search_input)
    EditText searchInput;

    @BindView(R.id.content_list)
    RecyclerView contentList;

    @BindView(R.id.empty_list)
    View emptyList;

    @BindView(R.id.action_add_resident)
    FloatingActionButton addResident;

    @BindView(R.id.progress_bar)
    View progressBar;


    @BindView(R.id.select_bar)
    View selectBar;

    @BindView(R.id.select_counter)
    TextView toolbarCounter;

    @BindView(R.id.toolbar_edit)
    View toolbarEdit;

    private MenuItem menuSign = null;
    private boolean isLogin = false;
    private Unbinder unbinder;
    private Resident firstSelectedItem;
    private boolean selectionMode = false;
    private List<Resident> residents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
        contentList.setAdapter(adapter);
        adapter.setListener(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        adapter = null;
        unbinder.unbind();
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchInput.addTextChangedListener(searchInputListener);
        presenter.getAllResident();
    }

    @Override
    protected void onStop() {
        searchInput.removeTextChangedListener(searchInputListener);
        addResident.setEnabled(true);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (menuSign != null) {
            presenter.checkLoginStatus();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menuSign = menu.findItem(R.id.action_login);
        presenter.checkLoginStatus();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                onClickMenuSearch();
                return true;

            case R.id.action_refresh:
                onClickMenuRefresh();
                return true;

            case R.id.action_login:
                onClickMenuLogin();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoginStatus(boolean isLogin) {
        this.isLogin = isLogin;
        if (menuSign != null) {
            menuSign.setTitle(isLogin ? getString(R.string.menu_logout) : getString(R.string.menu_login));
        }
        addResident.setVisibility(isLogin ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onResidentFetched(List<Resident> residents) {
        this.residents = residents;
        emptyList.setVisibility(View.GONE);
        adapter.setItems(residents);
    }

    @Override
    public void onEmptyResident() {
        emptyList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNewResidentCreated(boolean success) {
        toast(String.valueOf(success));
    }

    @Override
    public void onResidentDeleted(boolean success) {
        if (success) {
            presenter.getAllResident();
            toast(getString(R.string.alert_deleted));
            toggleSelectBar(false);
        } else {
            showSimpleSnackBar(getString(R.string.alert_delete_failed));
        }
    }

    @Override
    public void onResidentUpdated(boolean success) {
        toast(getString(R.string.alert_success));
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showSimpleSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int position, Resident resident) {
        if (selectionMode) {
            toggleItemSelection(position);
        } else {
            Intent i = new Intent(this, DetailActivity.class);
            i.putExtra(DetailActivity.EXTRA_RESIDENT_ID, resident.getId());
            startActivity(i);
        }

    }

    @Override
    public void onItemLongClick(int position, Resident resident) {
        if (!selectionMode) {
            toggleSelectBar(true);
            firstSelectedItem = resident;
            toolbarEdit.setVisibility(View.VISIBLE);
        }

        toggleItemSelection(position);
    }

    @OnClick(R.id.search_back)
    void onClickSearchBack() {
        searchInput.setText("");
        searchBar.setVisibility(View.GONE);
        SystemHelper.hideSoftKeyboard(this, searchInput);
    }

    @OnClick(R.id.search_clear)
    void onClickSearchClear() {
        searchInput.setText("");
    }

    @OnClick(R.id.action_add_resident)
    void onClickAddResident() {
        addResident.setEnabled(false);
        startActivity(new Intent(this, CreateActivity.class));
    }

    @OnClick(R.id.empty_list)
    void onClickEmptyList() {
        if (isLogin) {
            onClickAddResident();
        } else {
            onClickMenuLogin();
        }

    }


    @OnClick(R.id.select_back)
    void onClickSelectBack() {
        toggleSelectBar(false);
    }


    @OnClick(R.id.toolbar_edit)
    void onClickToolbarEdit() {
        toggleSelectBar(false);
        Intent i = new Intent(this, EditActivity.class);
        i.putExtra(EditActivity.EXTRA_RESIDENT_ID, firstSelectedItem.getId());
        startActivity(i);
    }

    @OnClick(R.id.toolbar_shortcut)
    void onClickToolbarShortcut() {
        for (int i : adapter.getSelectedItems()) {
            try {
                AppHelper.createResidentShortcut(getApplicationContext(), residents.get(i));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        toggleSelectBar(false);
    }

    @OnClick(R.id.toolbar_delete)
    void onClickToolbarDelete() {
        ViewHelper.showConfirmDialog(this, getString(R.string.title_delete), getString(R.string.alert_delete_resident), getString(R.string.title_delete), getString(R.string.title_cancel), (dialog, which) -> {
            if (residents != null && !residents.isEmpty()) {
                List<Integer> selectedPositions = adapter.getSelectedItems();
                List<Resident> delete = new ArrayList<>();
                for (int i : selectedPositions) {
                    delete.add(residents.get(i));
                }
                presenter.deleteMultipleResident(delete);
                toggleSelectBar(false);
            } else {
                showSimpleSnackBar(getString(R.string.alert_delete_failed));
            }
        });
    }

    private void onClickMenuSearch() {
        searchBar.setVisibility(View.VISIBLE);
        searchInput.requestFocus();
        SystemHelper.showSoftKeyboard(this, searchInput);
    }

    private void onClickMenuRefresh() {
        presenter.getAllResident();
    }

    private void onClickMenuLogin() {
        if (isLogin) {
            ViewHelper.showConfirmDialog(this, getString(R.string.menu_logout), getString(R.string.alert_logout), getString(R.string.menu_logout), getString(R.string.title_cancel), (dialog, which) ->
                    presenter.performLogout()
            );
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    private void onPerformFilter(String s) {
        adapter.getFilter().filter(s);
    }

    private TextWatcher searchInputListener = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            searchClear.setVisibility(s.toString().trim().length() <= 0 ? View.GONE : View.VISIBLE);
            onPerformFilter(s.toString());
        }
    };

    private void toggleItemSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();
        String counter = "";

        if (count > 0) {
            counter = "Selected: " + String.valueOf(count);
            toolbarEdit.setVisibility(count > 1 ? View.GONE : View.VISIBLE);
            firstSelectedItem = null;
            if (count == 1) {
                firstSelectedItem = residents.get(adapter.getSelectedItems().get(0));
            }
        } else {
            toggleSelectBar(false);
        }

        toolbarCounter.setText(counter);
    }

    private void toggleSelectBar(boolean selection) {
        selectBar.setVisibility(selection && isLogin ? View.VISIBLE : View.GONE);
        selectionMode = selection;
        if (!selection) {
            adapter.clearSelection();
        }
    }

}
