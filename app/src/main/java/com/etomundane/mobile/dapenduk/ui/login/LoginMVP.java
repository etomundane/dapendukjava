package com.etomundane.mobile.dapenduk.ui.login;

import android.support.v4.util.Pair;

import com.etomundane.mobile.dapenduk.base.BaseMVP;
import com.etomundane.mobile.dapenduk.data.local.database.user.User;

import io.reactivex.Single;

/**
 * Date Created: 2018-07-08 17:17:01
 * Author: Eto Mundane
 */
public interface LoginMVP {
    interface Mediator extends BaseMVP.Mediator {
        boolean isRememberPassword();

        Pair<String, String> getRememberedPassword();

        Single<User> obtainLogin(String username, String password);

        void updateCurrentAdmin(User user, boolean rememberPassword);
    }

    interface View extends BaseMVP.View {
        void onRememberPassword(boolean remember);

        void onPasswordRemembered(String username, String password);

        void onLoginSuccess();

        void onUserNotExisted();
    }

    interface Presenter<V extends LoginMVP.View, M extends LoginMVP.Mediator> extends BaseMVP.Presenter<V, M> {
        void isRememberPassword();

        void getRememberedPassword();

        void onSubmitLogin(String username, String password, boolean rememberPassword);
    }
}
