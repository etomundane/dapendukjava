package com.etomundane.mobile.dapenduk.util.common;

/**
 * Date Created: 2018-07-08 19:44:08
 * Author: Eto Mundane
 */
public abstract class Constants {
    public static final String DOB_DATE_FORMAT = "MMM dd, yyyy";
}
