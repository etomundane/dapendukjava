package com.etomundane.mobile.dapenduk.ui.splash;

import com.etomundane.mobile.dapenduk.base.BasePresenter;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Date Created: 2018-07-07 21:23:13
 * Author: Eto Mundane
 */
@ActivityScope
public class SplashPresenter<V extends SplashMVP.View, M extends SplashMVP.Mediator> extends BasePresenter<V, M> implements SplashMVP.Presenter<V, M> {

    @Inject
    SplashPresenter(M mMediator, AppScheduler mScheduler, CompositeDisposable mCompositeDisposable) {
        super(mMediator, mScheduler, mCompositeDisposable);
    }

    @Override
    public void initDefaultAdmin() {
        compositeDisposable.add(
                getMediator().isAdminEmpty()
                        .compose(scheduler.ioObservableScheduler())
                        .concatMap(
                                empty -> {
                                    if (empty) {
                                        insertDefaultAdmin();
                                        return Observable.just(true);
                                    } else {
                                        return Observable.just(false);
                                    }
                                })
                        .doOnError(e -> getView().showError(e.getLocalizedMessage()))
                        .subscribe(success -> getView().goToHome())
        );

    }

    private void insertDefaultAdmin() {
        getMediator().insertDefaultAdmin()
                .subscribeOn(scheduler.io())
                .subscribe();
    }
}
