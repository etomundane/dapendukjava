package com.etomundane.mobile.dapenduk.data.repository.user;

import com.etomundane.mobile.dapenduk.data.local.database.user.User;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-06 00:02:57
 * Author: Eto Mundane
 */
public interface UserRepository {
    Observable<Boolean> isAdminEmpty();

    Observable<Long> insertAdminUser(User userAdmin);

    Single<User> getAdminUser(long uid);

    Single<User> getAdminUser(String username, String password);
}
