package com.etomundane.mobile.dapenduk.data.local.database.resident;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Date Created: 2018-07-06 00:20:18
 * Author: Eto Mundane
 */
@Entity(tableName = Resident.TABLE_NAME)
public final class Resident {
    public static final String TABLE_NAME = "resident";
    public static final String C_ID = "id";
    private static final String C_FULL_NAME = "full_name";
    private static final String C_ADDRESS = "full_address";

    @ColumnInfo(name = Resident.C_FULL_NAME)
    private
    String fullName;

    @ColumnInfo(name = Resident.C_ADDRESS)
    private
    String fullAddress;

    private String gender;

    private String pob;

    private long dob;

    @ColumnInfo(name = Resident.C_ID)
    @PrimaryKey(autoGenerate = true)
    long id;

    private String photo;

    private String work;

    public interface Gender {
        String MALE = "Male";
        String FEMALE = "Female";
        String OTHER = "Other";
    }

    @Ignore
    public Resident(String fullName, String fullAddress, String gender, String pob, long dob) {
        this.fullName = fullName;
        this.fullAddress = fullAddress;
        this.gender = gender;
        this.pob = pob;
        this.dob = dob;
    }

    public Resident(String fullName, String fullAddress, String gender, String pob, long dob, long id, String photo, String work) {
        this.fullName = fullName;
        this.fullAddress = fullAddress;
        this.gender = gender;
        this.pob = pob;
        this.dob = dob;
        this.id = id;
        this.photo = photo;
        this.work = work;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getFullName() {
        return fullName;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public String getGender() {
        return gender;
    }

    public String getPob() {
        return pob;
    }

    public long getDob() {
        return dob;
    }

    public long getId() {
        return id;
    }

    public String getPhoto() {
        return photo;
    }

    public String getWork() {
        return work;
    }
}
