package com.etomundane.mobile.dapenduk.ui.edit;

import com.etomundane.mobile.dapenduk.base.BaseMVP;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-09 00:10:47
 * Author: Eto Mundane
 */
public interface EditMVP {
    interface Mediator extends BaseMVP.Mediator {
        Single<Resident> getResident(long id);

        Observable<Boolean> obtainEdit(Resident resident);
    }

    interface View extends BaseMVP.View {
        void onEdited(Resident resident, boolean success);

        void onResidentFetched(Resident resident);
    }

    interface Presenter<V extends EditMVP.View, M extends EditMVP.Mediator> extends BaseMVP.Presenter<V, M> {
        void getResidentById(long id);

        void obtainEdit(Resident resident, long id);
    }
}
