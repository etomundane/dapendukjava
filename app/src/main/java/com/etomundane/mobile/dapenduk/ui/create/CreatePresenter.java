package com.etomundane.mobile.dapenduk.ui.create;

import com.etomundane.mobile.dapenduk.base.BasePresenter;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Date Created: 2018-07-08 18:58:41
 * Author: Eto Mundane
 */
@ActivityScope
public class CreatePresenter<V extends CreateMVP.View, M extends CreateMVP.Mediator> extends BasePresenter<V, M> implements CreateMVP.Presenter<V, M> {

    @Inject
    CreatePresenter(M mMediator, AppScheduler scheduler, CompositeDisposable compositeDisposable) {
        super(mMediator, scheduler, compositeDisposable);
    }

    @Override
    public void onLeavePage(Resident unsavedResident) {
        getMediator().setUnsavedResident(unsavedResident);
    }

    @Override
    public void getUnsavedResident() {
        getView().onUnsavedResidentRestored(getMediator().getUnsavedResident());
    }

    @Override
    public void obtainCreate(Resident resident) {
        getView().showProgress(true);
        Disposable disposable = getMediator().obtainCreate(resident)
                .compose(scheduler.ioObservableScheduler())
                .doOnError(e -> getView().showError(e.getLocalizedMessage()))
                .subscribe(uid -> {
                            getMediator().clearUnsavedResident();
                            getView().onCreateSuccess(resident.getFullName());
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }
}
