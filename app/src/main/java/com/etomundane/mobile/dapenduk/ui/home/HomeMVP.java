package com.etomundane.mobile.dapenduk.ui.home;

import com.etomundane.mobile.dapenduk.base.BaseMVP;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-05 22:50:47
 * Author: Eto Mundane
 */
public interface HomeMVP {
    interface Mediator extends BaseMVP.Mediator {
        Single<List<Resident>> getAllResident();

        Observable<Long> createNewResident(Resident resident);

        Observable<Boolean> deleteResident(long id);

        Observable<Boolean> deleteMultipleResident(List<Resident> residents);

        Observable<Boolean> updateResident(Resident resident);
    }

    interface View extends BaseMVP.View {
        void onLoginStatus(boolean isLogin);

        void onResidentFetched(List<Resident> residents);

        void onEmptyResident();

        void onNewResidentCreated(boolean success);

        void onResidentDeleted(boolean success);

        void onResidentUpdated(boolean success);
    }

    interface Presenter<V extends View, M extends Mediator> extends BaseMVP.Presenter<V, M> {
        void checkLoginStatus();

        void performLogout();

        void getAllResident();

        void createNewResident(Resident resident);

        void deleteResident(long id);

        void deleteMultipleResident(List<Resident> residents);

        void updateResident(Resident resident);
    }
}
