package com.etomundane.mobile.dapenduk.ui.home;

import com.etomundane.mobile.dapenduk.base.BaseMediator;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;
import com.etomundane.mobile.dapenduk.data.repository.resident.ResidentRepository;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.di.qualifier.LocalData;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-08 01:59:48
 * Author: Eto Mundane
 */
@ActivityScope
public class HomeMediator extends BaseMediator implements HomeMVP.Mediator {

    private final ResidentRepository residentRepo;

    @Inject
    HomeMediator(@LocalData AppPreference pref, @Singleton ResidentRepository residentRepository) {
        super(pref);
        this.residentRepo = residentRepository;
    }

    @Override
    public Single<List<Resident>> getAllResident() {
        return residentRepo.getAllResident();
    }

    @Override
    public Observable<Long> createNewResident(Resident resident) {
        return residentRepo.persistResident(resident);
    }

    @Override
    public Observable<Boolean> deleteResident(long id) {
        return residentRepo.deleteResident(id);
    }

    @Override
    public Observable<Boolean> deleteMultipleResident(List<Resident> residents) {
        return residentRepo.deleteResidents(residents);
    }

    @Override
    public Observable<Boolean> updateResident(Resident resident) {
        return residentRepo.updateResident(resident);
    }
}