package com.etomundane.mobile.dapenduk.ui.login;

import android.support.v4.util.Pair;

import com.etomundane.mobile.dapenduk.base.BaseMediator;
import com.etomundane.mobile.dapenduk.data.local.database.user.User;
import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;
import com.etomundane.mobile.dapenduk.data.repository.user.UserRepository;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.di.qualifier.LocalData;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;


/**
 * Date Created: 2018-07-08 17:25:18
 * Author: Eto Mundane
 */
@ActivityScope
public class LoginMediator extends BaseMediator implements LoginMVP.Mediator {
    private UserRepository userRepo;

    @Inject
    LoginMediator(@LocalData AppPreference pref, @Singleton UserRepository userRepository) {
        super(pref);
        this.userRepo = userRepository;
    }

    @Override
    public boolean isRememberPassword() {
        return pref.isPasswordRemembered();
    }

    @Override
    public Pair<String, String> getRememberedPassword() {
        return new Pair<>(pref.getRememberedUsername(), pref.getRememberedPassword());
    }

    @Override
    public Single<User> obtainLogin(String username, String password) {
        return userRepo.getAdminUser(username, password);
    }

    @Override
    public void updateCurrentAdmin(User user, boolean rememberPassword) {
        pref.setLogin(true);
        pref.setRememberPassword(rememberPassword);
        if (rememberPassword) {
            pref.saveRememberedPassword(user.getUsername(), user.getPassword());
        }
    }
}
