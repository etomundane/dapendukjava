package com.etomundane.mobile.dapenduk.base;

import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Date Created: 2018-07-05 23:35:09
 * Author: Eto Mundane
 */
public abstract class BasePresenter<V extends BaseMVP.View, M extends BaseMVP.Mediator> implements BaseMVP.Presenter<V, M> {
    private V mView;
    private M mMediator;

    protected final AppScheduler scheduler;
    protected final CompositeDisposable compositeDisposable;

    public BasePresenter(M mMediator, AppScheduler scheduler, CompositeDisposable compositeDisposable) {
        this.mMediator = mMediator;
        this.scheduler = scheduler;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public V getView() {
        return mView;
    }

    @Override
    public M getMediator() {
        return mMediator;
    }

    @Override
    public void attachView(V view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
        mMediator = null;
        compositeDisposable.dispose();
    }

    public boolean isViewNull() {
        return mView == null;
    }
}
