package com.etomundane.mobile.dapenduk.data.repository.resident;

import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-06 00:03:59
 * Author: Eto Mundane
 */
public interface ResidentRepository {
    Observable<Long> persistResident(Resident resident);

    Observable<Boolean> deleteResident(long id);

    Observable<Boolean> deleteResidents(List<Resident> residents);

    Single<List<Resident>> getAllResident();

    Single<Resident> getResident(long id);

    Observable<Boolean> updateResident(Resident resident);
}
