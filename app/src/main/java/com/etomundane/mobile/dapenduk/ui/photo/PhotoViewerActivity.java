package com.etomundane.mobile.dapenduk.ui.photo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.etomundane.mobile.dapenduk.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PhotoViewerActivity extends AppCompatActivity {

    public static final String EXTRA_PATH = "extra_uri";

    @BindView(R.id.web_view)
    WebView webView;
    private Unbinder unbinder;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        unbinder = ButterKnife.bind(this);

        String extraPath = getIntent().getStringExtra(EXTRA_PATH);
        String data = "<html><head><style>img{display: inline;height: auto;max-width: 100%;margin: 0; padding: 0}</style></head><body> <img src=\"" + extraPath + "\"> </body></html>";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadDataWithBaseURL("", data, "text/html", "utf-8", "");
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @OnClick(R.id.back)
    void onClickBack() {
        onBackPressed();
    }


}
