package com.etomundane.mobile.dapenduk.base;

/**
 * Date Created: 2018-07-05 22:53:16
 * Author: Eto Mundane
 */
public interface BaseMVP {
    interface Mediator {
        boolean isAdminLogin();

        void performLogout();
    }

    interface View {
        void showProgress(boolean show);

        void showError(String message);

        void toast(String message);
    }

    interface Presenter<V extends View, M extends Mediator> {
        void attachView(V view);

        void detachView();

        V getView();

        M getMediator();
    }
}
