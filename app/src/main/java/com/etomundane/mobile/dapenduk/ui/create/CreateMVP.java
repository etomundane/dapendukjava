package com.etomundane.mobile.dapenduk.ui.create;

import android.support.annotation.Nullable;

import com.etomundane.mobile.dapenduk.base.BaseMVP;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

import io.reactivex.Observable;

/**
 * Date Created: 2018-07-08 18:47:43
 * Author: Eto Mundane
 */
public interface CreateMVP {
    interface Mediator extends BaseMVP.Mediator {
        void setUnsavedResident(Resident resident);

        Resident getUnsavedResident();

        void clearUnsavedResident();

        Observable<Long> obtainCreate(Resident resident);
    }

    interface View extends BaseMVP.View {
        void onUnsavedResidentRestored(@Nullable Resident unsavedResident);

        void onCreateSuccess(String fullName);
    }

    interface Presenter<V extends CreateMVP.View, M extends CreateMVP.Mediator> extends BaseMVP.Presenter<V, M> {
        void onLeavePage(Resident unsavedResident);

        void getUnsavedResident();

        void obtainCreate(Resident resident);
    }
}
