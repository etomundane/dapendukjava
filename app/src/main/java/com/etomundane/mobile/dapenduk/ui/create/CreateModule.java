package com.etomundane.mobile.dapenduk.ui.create;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * Date Created: 2018-07-08 19:11:12
 * Author: Eto Mundane
 */
@Module
public abstract class CreateModule {
    @ActivityScope
    @Binds
    abstract CreateMVP.Mediator createMediator(@ActivityScope CreateMediator mediator);

    @ActivityScope
    @Binds
    abstract CreateMVP.Presenter createPresenter(@ActivityScope CreatePresenter<CreateMVP.View, CreateMVP.Mediator> presenter);

}
