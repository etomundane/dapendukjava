package com.etomundane.mobile.dapenduk.ui.home;

import android.content.Context;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Date Created: 2018-07-05 22:41:43
 * Author: Eto Mundane
 */
@Module
public abstract class HomeModule {
    @ActivityScope
    @Binds
    abstract HomeMVP.Mediator homeMediator(@ActivityScope HomeMediator mediator);

    @ActivityScope
    @Binds
    abstract HomeMVP.Presenter homePresenter(@ActivityScope HomePresenter<HomeMVP.View, HomeMVP.Mediator> presenter);

    @ActivityScope
    @Binds
    abstract Context bindContext(HomeActivity context);

    @Provides
    @ActivityScope
    static ResidentListAdapter residentListAdapter(Context context) {
        return new ResidentListAdapter(context);
    }
}
