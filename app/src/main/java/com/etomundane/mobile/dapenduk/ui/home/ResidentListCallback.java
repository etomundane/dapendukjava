package com.etomundane.mobile.dapenduk.ui.home;

import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

/**
 * Date Created: 2018-07-08 14:38:24
 * Author: Eto Mundane
 */
interface ResidentListCallback {
    void onItemClick(int position, Resident resident);

    void onItemLongClick(int position, Resident resident);
}
