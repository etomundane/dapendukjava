package com.etomundane.mobile.dapenduk.ui.edit;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.ui.photo.PhotoViewerActivity;
import com.etomundane.mobile.dapenduk.util.android.SystemHelper;
import com.etomundane.mobile.dapenduk.util.android.ViewHelper;
import com.etomundane.mobile.dapenduk.util.common.Constants;
import com.robertlevonyan.components.picker.ItemModel;
import com.robertlevonyan.components.picker.PickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;
import timber.log.Timber;

public class EditActivity extends DaggerAppCompatActivity implements EditMVP.View {

    public static final String EXTRA_RESIDENT_ID = "extra_edit_id";
    private static final int REQUEST_MULTIPLE_PERMISSIONS = 111;

    @Inject
    EditPresenter<EditMVP.View, EditMVP.Mediator> presenter;

    @BindView(R.id.activity_edit)
    ViewGroup rootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    View progressBar;

    @BindView(R.id.icon_photo)
    ImageView iconPhoto;

    @BindView(R.id.et_full_name)
    EditText etFullName;

    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.sp_gender)
    AppCompatSpinner spGender;

    @BindView(R.id.et_pob)
    EditText etPob;

    @BindView(R.id.et_dob)
    EditText etDob;

    @BindView(R.id.et_work)
    EditText etWork;

    @BindView(R.id.bt_save)
    View vSave;

    @BindView(R.id.action_cancel)
    View vCancel;

    private Unbinder unbinder;
    private String[] permissionNeeded = {"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    private boolean allGranted = true;

    private long dobMillis = 0;
    private String photoUri = "";
    private long residentId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar.getNavigationIcon() != null) {
                toolbar.getNavigationIcon()
                        .setColorFilter(ContextCompat.getColor(this, android.R.color.black), PorterDuff.Mode.MULTIPLY);
            }
        }
        etDob.setFocusable(false);
        etDob.setClickable(true);

        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (photoUri.isEmpty()) {
                    iconPhoto.setImageResource(i == 2 ? R.drawable.ic_ph_person_female_80dp : R.drawable.ic_ph_person_male_80dp);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        residentId = getIntent().getLongExtra(EXTRA_RESIDENT_ID, 0);
        presenter.attachView(this);
        if (residentId > 0) {
            presenter.getResidentById(residentId);
        } else {
            toast(getString(R.string.alert_info_unavailable));
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        vSave.setEnabled(true);
        vCancel.setEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                onClickSave();
                return true;

            case android.R.id.home:
                toast(getString(R.string.alert_information_discarded));
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onEdited(Resident resident, boolean edited) {
        if (edited) {
            toast(getString(R.string.alert_success));
            finish();
        } else {
            vSave.setEnabled(true);
            vCancel.setEnabled(true);
            showSimpleSnackBar(getString(R.string.alert_edit_failed));
        }
    }

    @Override
    public void onResidentFetched(Resident resident) {
        dobMillis = resident.getDob();
        photoUri = resident.getPhoto();

        initLastForm(resident);
    }

    private void initLastForm(Resident resident) {
        etFullName.setText(resident.getFullName());
        etAddress.setText(resident.getFullAddress());
        spGender.setSelection(setSelectedGender(resident));
        etPob.setText(resident.getPob());
        etDob.setText(SystemHelper.millisToTime(dobMillis, Constants.DOB_DATE_FORMAT));
        etWork.setText(resident.getWork());
        if (!resident.getPhoto().isEmpty()) {
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .dontAnimate()
                    .placeholder(R.drawable.ic_ph_profile_photo_400dp)
                    .circleCrop();

            Timber.i("resident photo uri: %s", Uri.parse(resident.getPhoto()));
            Glide.with(this)
                    .load(Uri.parse(resident.getPhoto()))
                    .apply(requestOptions)
                    .into(iconPhoto);
        }
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(String message) {
        showSimpleSnackBar(message);
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showSimpleSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.bt_save)
    void onClickSave() {
        SystemHelper.hideSoftKeyboard(this, etFullName);
        resetErrorForms();
        Resident r = generateResident();
        if (r.getFullName().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_full_name_required));
            setEditTextError(etFullName, true);
        } else if (r.getFullAddress().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_address_required));
            setEditTextError(etAddress, true);
        } else if (r.getGender().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_select_a_gender));
            setEditTextError(spGender, true);
        } else if (r.getPob().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_pob_required));
            setEditTextError(etPob, true);
        } else if (etDob.getText().toString().trim().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_choose_dob));
            setEditTextError(etDob, true);
        } else {
            vSave.setEnabled(false);
            vCancel.setEnabled(false);
            presenter.obtainEdit(r, residentId);
        }
    }

    @OnClick(R.id.et_dob)
    void onClickDOB() {
        showDatePicker(getString(R.string.label_dob), dobMillis, (view, year, monthOfYear, dayOfMonth) -> {
            dobMillis = SystemHelper.calendarToMillis(year, monthOfYear, dayOfMonth);
            etDob.setText(SystemHelper.millisToTime(dobMillis, Constants.DOB_DATE_FORMAT));
        });
    }

    @OnClick(R.id.edit_photo)
    void onClickEditPhoto() {
        boolean hasPermission = SystemHelper.hasPermissions(this, permissionNeeded);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this, permissionNeeded, REQUEST_MULTIPLE_PERMISSIONS);
        } else {
            pickPhoto();
        }
    }

    @OnClick(R.id.icon_photo)
    void onClickIconPhoto() {
        if (photoUri.isEmpty()) {
            onClickEditPhoto();
        } else {
            Intent i = new Intent(this, PhotoViewerActivity.class);
            i.putExtra(PhotoViewerActivity.EXTRA_PATH, photoUri);
            startActivity(i);
        }
    }

    @OnClick(R.id.action_cancel)
    void onClickCancel() {
        ViewHelper.showConfirmDialog(this, getString(R.string.title_cancel), getString(R.string.alert_discard_changes), getString(android.R.string.ok), getString(R.string.title_cancel), (dialog, which) ->
                finish()
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_MULTIPLE_PERMISSIONS:
                int total = grantResults.length;
                if (total > 0) {
                    for (int grantResult : grantResults) {
                        boolean granted = grantResult == PackageManager.PERMISSION_GRANTED;
                        allGranted = allGranted && granted;
                    }

                    if (allGranted) {
                        pickPhoto();
                    } else {
                        ViewHelper.showConfirmDialog(this, getString(R.string.title_permission_needed), getString(R.string.alert_permissions_needed), getString(R.string.prompt_setting), getString(R.string.prompt_ignore), (dialog, which) -> SystemHelper.goToApplicationSettings(this));
                    }
                }
                break;
        }
    }

    private void pickPhoto() {
        int accent = ContextCompat.getColor(this, R.color.accent);
        ItemModel itemCamera = new ItemModel(ItemModel.ITEM_CAMERA, "", R.drawable.ic_camera_white_24dp, true, ItemModel.TYPE_CIRCLE, accent);
        ItemModel itemGallery = new ItemModel(ItemModel.ITEM_GALLERY, "", R.drawable.ic_gallery_white_24dp, true, ItemModel.TYPE_CIRCLE, accent);
        ArrayList<ItemModel> itemModels = new ArrayList<>();
        itemModels.add(itemCamera);
        itemModels.add(itemGallery);
        PickerDialog pickerDialog = new PickerDialog.Builder(this)
                .setTitle("")
                .setListType(PickerDialog.TYPE_GRID, 2)
                .setItems(itemModels)
                .setDialogStyle(PickerDialog.DIALOG_STANDARD)
                .create();

        pickerDialog.setOnPickerCloseListener((type, uri) -> {
                    photoUri = uri == null ? "" : uri.toString();
                    initLastForm(generateResident());
                }
        );
        pickerDialog.show();
    }

    private void resetErrorForms() {
        setEditTextError(etFullName, false);
        setEditTextError(etAddress, false);
        setEditTextError(spGender, false);
        setEditTextError(etPob, false);
        setEditTextError(etDob, false);
    }

    private Resident generateResident() {
        String fullName = etFullName.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String gender = getSelectedGender();
        String pob = etPob.getText().toString().trim();
        String work = etWork.getText().toString().trim();
        Resident resident = new Resident(fullName, address, gender, pob, dobMillis);
        resident.setWork(work);
        resident.setPhoto(photoUri);
        return resident;
    }


    private String getSelectedGender() {
        int selectedPosition = spGender.getSelectedItemPosition();
        String result = "";
        switch (selectedPosition) {
            case 1:
                result = Resident.Gender.MALE;
                break;
            case 2:
                result = Resident.Gender.FEMALE;
                break;
            case 3:
                result = Resident.Gender.OTHER;
                break;
        }

        return result;
    }

    private int setSelectedGender(Resident unsavedResident) {
        int result = 0;
        if (unsavedResident.getGender().matches(Resident.Gender.MALE)) {
            result = 1;
        } else if (unsavedResident.getGender().matches(Resident.Gender.FEMALE)) {
            result = 2;
        } else if (unsavedResident.getGender().matches(Resident.Gender.OTHER)) {
            result = 3;
        }
        return result;
    }

    private void setEditTextError(View editText, boolean isError) {
        editText.setBackground(isError ?
                ContextCompat.getDrawable(this, R.drawable.background_my_edit_text_error) :
                ContextCompat.getDrawable(this, R.drawable.background_my_edit_text)
        );
    }

    private void showDatePicker(String title, long currentDate, DatePickerDialog.OnDateSetListener callback) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(currentDate);
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                callback,
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setTitle(title);
        dpd.setMaxDate(Calendar.getInstance());
        dpd.show(getFragmentManager(), "FRAGMENT_DATE_PICKER");
    }

}
