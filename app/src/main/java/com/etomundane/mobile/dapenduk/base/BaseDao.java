package com.etomundane.mobile.dapenduk.base;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Date Created: 2018-07-05 23:48:12
 * Author: Eto Mundane
 */
public interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(T obj);

    @Update
    int update(T obj);

    @Update
    int update(List<T> listObj);

    @Delete
    int delete(T obj);

    @Delete
    int delete(List<T> listObj);

}
