package com.etomundane.mobile.dapenduk.di.module;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.ui.create.CreateActivity;
import com.etomundane.mobile.dapenduk.ui.create.CreateModule;
import com.etomundane.mobile.dapenduk.ui.detail.DetailActivity;
import com.etomundane.mobile.dapenduk.ui.detail.DetailModule;
import com.etomundane.mobile.dapenduk.ui.edit.EditActivity;
import com.etomundane.mobile.dapenduk.ui.edit.EditModule;
import com.etomundane.mobile.dapenduk.ui.home.HomeActivity;
import com.etomundane.mobile.dapenduk.ui.home.HomeModule;
import com.etomundane.mobile.dapenduk.ui.login.LoginActivity;
import com.etomundane.mobile.dapenduk.ui.login.LoginModule;
import com.etomundane.mobile.dapenduk.ui.splash.SplashActivity;
import com.etomundane.mobile.dapenduk.ui.splash.SplashModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Date Created: 2018-07-05 22:40:35
 * Author: Eto Mundane
 */
@Module
public abstract class ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector(modules = SplashModule.class)
    abstract SplashActivity splashActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeActivity homeActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginModule.class)
    abstract LoginActivity loginActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = CreateModule.class)
    abstract CreateActivity createActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = DetailModule.class)
    abstract DetailActivity detailActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = EditModule.class)
    abstract EditActivity editActivity();
}
