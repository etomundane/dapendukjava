package com.etomundane.mobile.dapenduk.util.rx;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;

/**
 * Date Created: 2018-07-07 12:04:26
 * Author: Eto Mundane
 */
public interface AppScheduler {
    Scheduler io();

    Scheduler computation();

    Scheduler ui();


    <T> ObservableTransformer<T, T> ioObservableScheduler();

    <T> SingleTransformer<T, T> ioSingleScheduler();

    <T> FlowableTransformer<T, T> ioFlowableScheduler();

    <T> MaybeTransformer<T, T> ioMaybeScheduler();

    CompletableTransformer ioCompletableScheduler();


    <T> ObservableTransformer<T, T> computationObservableScheduler();

    <T> SingleTransformer<T, T> computationSingleScheduler();

    <T> FlowableTransformer<T, T> computationFlowableScheduler();

    <T> MaybeTransformer<T, T> computationMaybeScheduler();

    CompletableTransformer computationCompletableScheduler();
}
