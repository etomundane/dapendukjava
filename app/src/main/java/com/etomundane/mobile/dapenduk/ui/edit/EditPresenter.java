package com.etomundane.mobile.dapenduk.ui.edit;

import com.etomundane.mobile.dapenduk.base.BasePresenter;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Date Created: 2018-07-09 00:17:12
 * Author: Eto Mundane
 */
@ActivityScope
public class EditPresenter<V extends EditMVP.View, M extends EditMVP.Mediator> extends BasePresenter<V, M> implements EditMVP.Presenter<V, M> {

    @Inject
    EditPresenter(M mMediator, AppScheduler scheduler, CompositeDisposable compositeDisposable) {
        super(mMediator, scheduler, compositeDisposable);
    }

    @Override
    public void getResidentById(long id) {
        getView().showProgress(true);
        Disposable disposable = getMediator().getResident(id)
                .compose(scheduler.ioSingleScheduler())
                .subscribe(
                        (resident, throwable) -> {
                            if (throwable != null) {
                                getView().showError(throwable.getLocalizedMessage());
                            } else if (resident != null) {
                                getView().onResidentFetched(resident);
                            }
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void obtainEdit(Resident resident, long id) {
        getView().showProgress(true);
        resident.setId(id);
        Disposable disposable = getMediator().obtainEdit(resident)
                .compose(scheduler.ioObservableScheduler())
                .subscribe(edited -> {
                            getView().onEdited(resident, edited);
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }
}
