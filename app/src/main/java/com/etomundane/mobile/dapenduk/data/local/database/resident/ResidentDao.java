package com.etomundane.mobile.dapenduk.data.local.database.resident;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.etomundane.mobile.dapenduk.base.BaseDao;

import java.util.List;

/**
 * Date Created: 2018-07-06 00:49:01
 * Author: Eto Mundane
 */
@Dao
public interface ResidentDao extends BaseDao<Resident> {
    @Query("select * from " + Resident.TABLE_NAME)
    List<Resident> getAllResident();

    @Query("select * from " + Resident.TABLE_NAME + " where " + Resident.C_ID + "=:id")
    Resident getResident(long id);

    @Query("delete from  " + Resident.TABLE_NAME + " where " + Resident.C_ID + "= :id")
    int deleteResident(long id);

    @Query("delete from " + Resident.TABLE_NAME)
    void clearTable();
}
