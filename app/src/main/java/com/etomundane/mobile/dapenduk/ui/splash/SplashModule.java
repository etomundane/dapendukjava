package com.etomundane.mobile.dapenduk.ui.splash;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * Date Created: 2018-07-07 22:41:31
 * Author: Eto Mundane
 */
@Module
public abstract class SplashModule {

    @ActivityScope
    @Binds
    abstract SplashMVP.Mediator splashMediator(@ActivityScope SplashMediator mediator);

    @ActivityScope
    @Binds
    abstract SplashMVP.Presenter splashPresenter(@ActivityScope SplashPresenter<SplashMVP.View, SplashMVP.Mediator> presenter);
}
