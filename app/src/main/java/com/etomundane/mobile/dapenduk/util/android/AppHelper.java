package com.etomundane.mobile.dapenduk.util.android;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Icon;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;

import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.ui.detail.DetailActivity;

import java.io.IOException;
import java.util.Objects;

public abstract class AppHelper {

    public static void createResidentShortcut(Context context, Resident resident) throws IOException {
        final String EXTRA_PHOTO_URI = "uri";
        int iconPlaceholder = resident.getGender().matches(Resident.Gender.FEMALE) ? R.drawable.ic_ph_person_female_80dp : R.drawable.ic_ph_person_male_80dp;

        Intent shortcutIntent = new Intent(context, DetailActivity.class);
        shortcutIntent.putExtra(DetailActivity.EXTRA_RESIDENT_ID, resident.getId());
        shortcutIntent.setAction(Intent.ACTION_MAIN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            if (shortcutManager != null && shortcutManager.isRequestPinShortcutSupported()) {
                boolean existedWithSameName = shortcutManager.getPinnedShortcuts().stream().anyMatch(s -> s.getId().matches(String.valueOf(resident.getId())) && Objects.equals(s.getShortLabel(), resident.getFullName()));
                boolean existedWithSameIcon = shortcutManager.getPinnedShortcuts().stream().anyMatch(s -> s.getId().matches(String.valueOf(resident.getId())) && Objects.requireNonNull(s.getExtras()).getString(EXTRA_PHOTO_URI, "").matches(resident.getPhoto()));
                boolean shouldNotCreateNew = existedWithSameName && existedWithSameIcon;

                ShortcutInfo.Builder builder;
                if (shouldNotCreateNew) {
                    builder = new ShortcutInfo.Builder(context, generateShortcutId(String.valueOf(resident.getId())));
                } else {
                    builder = new ShortcutInfo.Builder(context, generateShortcutId(String.valueOf(resident.getId() + System.currentTimeMillis())));
                }

                Icon icon = resident.getPhoto().isEmpty() ? Icon.createWithResource(context, iconPlaceholder) : Icon.createWithBitmap(createCircleIcon(context, Uri.parse(resident.getPhoto())));

                PersistableBundle extras = new PersistableBundle();
                extras.putString(EXTRA_PHOTO_URI, resident.getPhoto());

                ShortcutInfo shortcut = builder.setIntent(shortcutIntent)
                        .setShortLabel(resident.getFullName())
                        .setIcon(icon)
                        .setExtras(extras)
                        .build();

                Intent pinnedShortcutCallbackIntent = shortcutManager.createShortcutResultIntent(shortcut);
                PendingIntent successCallback = PendingIntent.getBroadcast(context, 0, pinnedShortcutCallbackIntent, 0);
                shortcutManager.requestPinShortcut(shortcut, successCallback.getIntentSender());
            }

        } else {
            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, resident.getFullName());
            addIntent.putExtra("duplicate", false);
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            if (resident.getPhoto().isEmpty()) {
                Bitmap photoPlaceholder = createCircleIcon(context, iconPlaceholder);
                if (photoPlaceholder != null) {
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, photoPlaceholder);
                } else {
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(context, R.mipmap.ic_launcher));
                }
            } else {
                Bitmap photo = createCircleIcon(context, Uri.parse(resident.getPhoto()));
                addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, photo);
            }

            context.sendBroadcast(addIntent);
        }

    }

    private static String generateShortcutId(String id) {
        return "shortcut_" + id;
    }

    private static Bitmap createCircleIcon(Context context, int vectorDrawable) {
        Bitmap bitmap = getBitmapFromVectorDrawable(context, vectorDrawable);
        if (bitmap != null) {
            return createCircleIcon(context, bitmap);
        } else {
            return null;
        }
    }

    private static Bitmap createCircleIcon(Context context, Uri uri) throws IOException {
        Bitmap bitmapInput = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        int dimension = Math.min(bitmapInput.getWidth(), bitmapInput.getHeight());
        Bitmap squareBitmap = ThumbnailUtils.extractThumbnail(bitmapInput, dimension, dimension);
        return createCircleIcon(context, squareBitmap);
    }

    private static Bitmap createCircleIcon(Context context, Bitmap input) {
        Bitmap output = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, input.getWidth(), input.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(input.getWidth() / 2, input.getHeight() / 2, input.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(input, rect, rect, paint);

        int iconSize = getDefaultIconSize(context);
        return Bitmap.createScaledBitmap(output, iconSize, iconSize, false);
    }

    private static int getDefaultIconSize(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        return am != null ? am.getLauncherLargeIconSize() : 72;
    }

    private static Bitmap getBitmapFromVectorDrawable(Context context, @DrawableRes int drawableResId) {
        VectorDrawableCompat drawable = VectorDrawableCompat.create(context.getResources(), drawableResId, null);
        Bitmap bitmap;
        if (drawable != null) {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                    drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } else {
            return null;
        }
    }

}
