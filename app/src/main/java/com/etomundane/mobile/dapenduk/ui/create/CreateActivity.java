package com.etomundane.mobile.dapenduk.ui.create;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.ui.photo.PhotoViewerActivity;
import com.etomundane.mobile.dapenduk.util.common.Constants;
import com.etomundane.mobile.dapenduk.util.android.SystemHelper;
import com.etomundane.mobile.dapenduk.util.android.ViewHelper;
import com.robertlevonyan.components.picker.ItemModel;
import com.robertlevonyan.components.picker.PickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;
import timber.log.Timber;

public class CreateActivity extends DaggerAppCompatActivity implements CreateMVP.View {
    private static final int REQUEST_MULTIPLE_PERMISSIONS = 111;

    @Inject
    CreatePresenter<CreateMVP.View, CreateMVP.Mediator> presenter;

    @BindView(R.id.activity_create)
    ViewGroup rootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    View progressBar;

    @BindView(R.id.icon_photo)
    ImageView iconPhoto;

    @BindView(R.id.et_full_name)
    EditText etFullName;

    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.sp_gender)
    AppCompatSpinner spGender;

    @BindView(R.id.et_pob)
    EditText etPob;

    @BindView(R.id.et_dob)
    EditText etDob;

    @BindView(R.id.et_work)
    EditText etWork;


    private Unbinder unbinder;
    private String[] permissionNeeded = {"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    private boolean allGranted = true;
    private long dobMillis = 0;
    private String photoUri = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar.getNavigationIcon() != null) {
                toolbar.getNavigationIcon()
                        .setColorFilter(ContextCompat.getColor(this, android.R.color.black), PorterDuff.Mode.MULTIPLY);
            }
        }

        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (photoUri.isEmpty()) {
                    iconPhoto.setImageResource(i == 2 ? R.drawable.ic_ph_person_female_80dp : R.drawable.ic_ph_person_male_80dp);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        etDob.setFocusable(false);
        etDob.setClickable(true);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        presenter.onLeavePage(generateResident());
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUnsavedResidentRestored(@Nullable Resident unsavedResident) {
        if (unsavedResident != null) {
            dobMillis = unsavedResident.getDob();
            photoUri = unsavedResident.getPhoto();

            initLastForm(unsavedResident);
        }
    }

    @Override
    public void onCreateSuccess(String fullName) {
        toast(getString(R.string.alert_dynamic_success_create_resident, fullName));
        finish();
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(String message) {
        showSimpleSnackBar(message);
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showSimpleSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.bt_create)
    void onClickCreate() {
        SystemHelper.hideSoftKeyboard(this, etFullName);
        resetErrorForms();
        Resident r = generateResident();
        if (r.getFullName().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_full_name_required));
            setEditTextError(etFullName, true);
        } else if (r.getFullAddress().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_address_required));
            setEditTextError(etAddress, true);
        } else if (r.getGender().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_select_a_gender));
            setEditTextError(spGender, true);
        } else if (r.getPob().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_pob_required));
            setEditTextError(etPob, true);
        } else if (etDob.getText().toString().trim().isEmpty()) {
            showSimpleSnackBar(getString(R.string.alert_choose_dob));
            setEditTextError(etDob, true);
        } else {
            presenter.obtainCreate(r);
        }
    }

    @OnClick(R.id.et_dob)
    void onClickDOB() {
        showDatePicker(getString(R.string.label_dob), dobMillis, (view, year, monthOfYear, dayOfMonth) -> {
            dobMillis = SystemHelper.calendarToMillis(year, monthOfYear, dayOfMonth);
            etDob.setText(SystemHelper.millisToTime(dobMillis, Constants.DOB_DATE_FORMAT));
        });
    }

    @OnClick(R.id.edit_photo)
    void onClickEditPhoto() {
        boolean hasPermission = SystemHelper.hasPermissions(this, permissionNeeded);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this, permissionNeeded, REQUEST_MULTIPLE_PERMISSIONS);
        } else {
            pickPhoto();
        }
    }

    @OnClick(R.id.icon_photo)
    void onClickIconPhoto() {
        if (photoUri.isEmpty()) {
            onClickEditPhoto();
        } else {
            Intent i = new Intent(this, PhotoViewerActivity.class);
            i.putExtra(PhotoViewerActivity.EXTRA_PATH, photoUri);
            startActivity(i);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_MULTIPLE_PERMISSIONS:
                int total = grantResults.length;
                if (total > 0) {
                    for (int grantResult : grantResults) {
                        boolean granted = grantResult == PackageManager.PERMISSION_GRANTED;
                        allGranted = allGranted && granted;
                    }

                    if (allGranted) {
                        pickPhoto();
                    } else {
                        ViewHelper.showConfirmDialog(this, getString(R.string.title_permission_needed), getString(R.string.alert_permissions_needed), getString(R.string.prompt_setting), getString(R.string.prompt_ignore), (dialog, which) -> SystemHelper.goToApplicationSettings(CreateActivity.this));
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void pickPhoto() {
        int accent = ContextCompat.getColor(this, R.color.accent);
        ItemModel itemCamera = new ItemModel(ItemModel.ITEM_CAMERA, "", R.drawable.ic_camera_white_24dp, true, ItemModel.TYPE_CIRCLE, accent);
        ItemModel itemGallery = new ItemModel(ItemModel.ITEM_GALLERY, "", R.drawable.ic_gallery_white_24dp, true, ItemModel.TYPE_CIRCLE, accent);
        ArrayList<ItemModel> itemModels = new ArrayList<>();
        itemModels.add(itemCamera);
        itemModels.add(itemGallery);
        PickerDialog pickerDialog = new PickerDialog.Builder(this)
                .setTitle("")
                .setListType(PickerDialog.TYPE_GRID, 2)
                .setItems(itemModels)
                .setDialogStyle(PickerDialog.DIALOG_STANDARD)
                .create();

        pickerDialog.setOnPickerCloseListener((type, uri) -> {
                    photoUri = uri == null ? "" : uri.toString();
                    initLastForm(generateResident());
                }
        );
        pickerDialog.show();
    }

    private void initLastForm(Resident resident) {
        etFullName.setText(resident.getFullName());
        etAddress.setText(resident.getFullAddress());
        spGender.setSelection(setSelectedGender(resident));
        etPob.setText(resident.getPob());
        etDob.setText(SystemHelper.millisToTime(dobMillis, Constants.DOB_DATE_FORMAT));
        etWork.setText(resident.getWork());
        if (!resident.getPhoto().isEmpty()) {
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .dontAnimate()
                    .circleCrop();

            Timber.i("resident photo uri: %s", Uri.parse(resident.getPhoto()));
            Glide.with(this)
                    .load(Uri.parse(resident.getPhoto()))
                    .apply(requestOptions)
                    .into(iconPhoto);
        }
    }

    private void resetErrorForms() {
        setEditTextError(etFullName, false);
        setEditTextError(etAddress, false);
        setEditTextError(spGender, false);
        setEditTextError(etPob, false);
        setEditTextError(etDob, false);
    }

    private Resident generateResident() {
        String fullName = etFullName.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String gender = getSelectedGender();
        String pob = etPob.getText().toString().trim();
        String work = etWork.getText().toString().trim();
        Resident resident = new Resident(fullName, address, gender, pob, dobMillis);
        resident.setWork(work);
        resident.setPhoto(photoUri);
        return resident;
    }

    private String getSelectedGender() {
        int selectedPosition = spGender.getSelectedItemPosition();
        String result = "";
        switch (selectedPosition) {
            case 1:
                result = Resident.Gender.MALE;
                break;
            case 2:
                result = Resident.Gender.FEMALE;
                break;
            case 3:
                result = Resident.Gender.OTHER;
                break;
        }

        return result;
    }

    private int setSelectedGender(Resident unsavedResident) {
        int result = 0;
        if (unsavedResident.getGender().matches(Resident.Gender.MALE)) {
            result = 1;
        } else if (unsavedResident.getGender().matches(Resident.Gender.FEMALE)) {
            result = 2;
        } else if (unsavedResident.getGender().matches(Resident.Gender.OTHER)) {
            result = 3;
        }
        return result;
    }

    private void setEditTextError(View editText, boolean isError) {
        editText.setBackground(isError ?
                ContextCompat.getDrawable(this, R.drawable.background_my_edit_text_error) :
                ContextCompat.getDrawable(this, R.drawable.background_my_edit_text)
        );
    }

    private void showDatePicker(String title, long currentDate, DatePickerDialog.OnDateSetListener callback) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(currentDate);
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                callback,
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setTitle(title);
        dpd.setMaxDate(Calendar.getInstance());
        dpd.show(getFragmentManager(), "FRAGMENT_DATE_PICKER");
    }
}
