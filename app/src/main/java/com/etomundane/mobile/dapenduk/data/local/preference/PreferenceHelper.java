package com.etomundane.mobile.dapenduk.data.local.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.di.qualifier.ApplicationScope;
import com.etomundane.mobile.dapenduk.di.qualifier.PreferenceInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Date Created: 2018-07-06 01:06:51
 * Author: Eto Mundane
 */
@Singleton
public class PreferenceHelper implements AppPreference {
    public static final String PREF_NAME = "Fr6TwUjFQ7GBRHIAalNyi7aee4c8oHJA";
    private static final String IS_LOGIN = "K5zP8Mup4bUASQTwLj5ADy5Z0pmTxKSw";
    private static final String UID = "bG86UjZadYNxUhI5mdxaDRJHNrsvM0tQ";
    private static final String IS_REMEMBER_PASSWORD = "Sb2yHSaOJHBu0ndxfLxrkgWBLqusB1Gg";
    private static final String REMEMBERED_USERNAME = "GkEmgUKGPuu2cYA7hYbBRbG5CuAzDmJy";
    private static final String REMEMBERED_PASSWORD = "61i8uO5ILVLnjcJVESNXf9NULfpDGrqc";
    private static final String UNSAVED_FULL_NAME = "OV8TOG68msjU7NJbkJX9a9YBR34CUrrS";
    private static final String UNSAVED_ADDRESS = "WNY4cVmIsFf9ngvgyT3YBtuDScJsnZ4l";
    private static final String UNSAVED_GENDER = "NUXuNeITZM5gBJJ9UQ390eopQIoGJKl1";
    private static final String UNSAVED_POB = "q1GkpqZjCK5siPXsn6C4iPLmWfyZhGHe";
    private static final String UNSAVED_DOB = "Hb4LJDkkbywF0I0eoKRNg4mdphyfo5lm";
    private static final String UNSAVED_PHOTO = "mCZR8fZn53JlI8HEVCQFUpWO0JjeJZCW";
    private static final String UNSAVED_WORK = "u0NfA7W2fJRUJVwoQBG8b1TFh3n28BvI";

    private SharedPreferences prefs;

    @Inject
    public PreferenceHelper(@ApplicationScope Context context, @PreferenceInfo String prefFileName) {
        prefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void setLogin(boolean isLogin) {
        prefs.edit().putLong(UID, 0).apply();
        prefs.edit().putBoolean(IS_LOGIN, isLogin).apply();
    }

    @Override
    public boolean isLogin() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

    @Override
    public void setUserId(long uid) {
        prefs.edit().putLong(UID, uid).apply();
    }

    @Override
    public long getCurrentUserId() {
        return prefs.getLong(UID, 0);
    }

    @Override
    public void setRememberPassword(boolean remember) {
        prefs.edit().putBoolean(IS_REMEMBER_PASSWORD, remember).apply();
    }

    @Override
    public boolean isPasswordRemembered() {
        return prefs.getBoolean(IS_REMEMBER_PASSWORD, false);
    }

    @Override
    public void saveRememberedPassword(String username, String password) {
        prefs.edit().putString(REMEMBERED_USERNAME, username).apply();
        prefs.edit().putString(REMEMBERED_PASSWORD, password).apply();
    }

    @Override
    public void clearRememberedPassword() {
        prefs.edit().putString(REMEMBERED_USERNAME, "").apply();
        prefs.edit().putString(REMEMBERED_PASSWORD, "").apply();
    }

    @Override
    public String getRememberedUsername() {
        return prefs.getString(REMEMBERED_USERNAME, "");
    }

    @Override
    public String getRememberedPassword() {
        return prefs.getString(REMEMBERED_PASSWORD, "");
    }

    @Override
    public void setUnsavedResident(Resident resident) {
        prefs.edit().putString(UNSAVED_FULL_NAME, resident.getFullName()).apply();
        prefs.edit().putString(UNSAVED_ADDRESS, resident.getFullAddress()).apply();
        prefs.edit().putString(UNSAVED_GENDER, resident.getGender()).apply();
        prefs.edit().putString(UNSAVED_POB, resident.getPob()).apply();
        prefs.edit().putLong(UNSAVED_DOB, resident.getDob()).apply();
        prefs.edit().putString(UNSAVED_PHOTO, resident.getPhoto()).apply();
        prefs.edit().putString(UNSAVED_WORK, resident.getWork()).apply();
    }

    @Override
    public Resident getUnsavedResident() {
        String fullName = prefs.getString(UNSAVED_FULL_NAME, "");
        String address = prefs.getString(UNSAVED_ADDRESS, "");
        String gender = prefs.getString(UNSAVED_GENDER, "");
        String pob = prefs.getString(UNSAVED_POB, "");
        long dob = prefs.getLong(UNSAVED_DOB, 0);
        String photo = prefs.getString(UNSAVED_PHOTO, "");
        String work = prefs.getString(UNSAVED_WORK, "");
        Resident resident = new Resident(fullName, address, gender, pob, dob);
        resident.setPhoto(photo);
        resident.setWork(work);
        return resident;
    }

    @Override
    public void clearUnsavedResident() {
        prefs.edit().putString(UNSAVED_FULL_NAME, "").apply();
        prefs.edit().putString(UNSAVED_ADDRESS, "").apply();
        prefs.edit().putString(UNSAVED_GENDER, "").apply();
        prefs.edit().putString(UNSAVED_POB, "").apply();
        prefs.edit().putLong(UNSAVED_DOB, 0).apply();
        prefs.edit().putString(UNSAVED_PHOTO, "").apply();
        prefs.edit().putString(UNSAVED_WORK, "").apply();
    }
}
