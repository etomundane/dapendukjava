package com.etomundane.mobile.dapenduk.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.ui.common.SelectableAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Date Created: 2018-07-08 14:33:56
 * Author: Eto Mundane
 */
@ActivityScope
public class ResidentListAdapter extends SelectableAdapter<ResidentListAdapter.ItemHolder> implements Filterable {
    private final int maleIcon, femaleIcon, colorAccent;
    private Context context;
    private ResidentListCallback listener;
    private List<Resident> items;
    private List<Resident> searchItems;
    private ResidentFilter itemFilter;
    private String filterQuery;

    private int selectedBackgroundColor;

    @Inject
    ResidentListAdapter(Context context) {
        this.context = context;
        this.selectedBackgroundColor = ContextCompat.getColor(context, R.color.selected_item_list);
        items = new ArrayList<>();
        searchItems = new ArrayList<>();
        maleIcon = R.drawable.ic_ph_person_male_80dp;
        femaleIcon = R.drawable.ic_ph_person_female_80dp;
        colorAccent = ContextCompat.getColor(context, R.color.accent);
    }

    public void setListener(ResidentListCallback listener) {
        this.listener = listener;
    }

    public void setItems(List<Resident> items) {
        this.items = items;
        this.searchItems = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_resident, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        try {
            final Resident item = items.get(position);
            holder.bindItem(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public Filter getFilter() {
        if (itemFilter == null) {
            itemFilter = new ResidentFilter(this, items);
        }
        return itemFilter;
    }

    private SpannableString setColorSpan(String source, String span, int color) {
        SpannableString result = new SpannableString(source);
        List<Integer> matchesIndexes = getFirstMatchedIndexes(source, span);
        if (matchesIndexes.size() > 0) {
            for (Integer i : matchesIndexes) {
                result.setSpan(new ForegroundColorSpan(color), i, i + span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        return result;
    }

    private List<Integer> getFirstMatchedIndexes(String text, String query) {
        List<Integer> result = new ArrayList<>();

        if (query != null && query.length() > 0) {
            for (int i = -1; (i = text.toLowerCase().indexOf(query.toLowerCase(), i + 1)) != -1; ) {
                result.add(i);
            }
        }

        return result;
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.photo)
        ImageView photo;

        @BindView(R.id.full_name)
        TextView fullName;

        @BindView(R.id.address)
        TextView address;

        @BindView(R.id.work)
        TextView work;

        @BindView(R.id.divider)
        View divider;

        ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> listener.onItemClick(getAdapterPosition(), items.get(getAdapterPosition())));

            itemView.setOnLongClickListener(view -> {
                listener.onItemLongClick(getAdapterPosition(), items.get(getAdapterPosition()));
                return true;
            });
        }

        void bindItem(Resident resident) {
            int placeHolder = resident.getGender().matches(Resident.Gender.FEMALE) ? femaleIcon : maleIcon;
            if (resident.getPhoto().isEmpty()) {
                photo.setImageResource(placeHolder);
            } else {
                RequestOptions requestOptions = new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .dontAnimate()
                        .placeholder(placeHolder)
                        .circleCrop();

                Timber.i("resident photo uri: %s", Uri.parse(resident.getPhoto()));
                Glide.with(context)
                        .load(Uri.parse(resident.getPhoto()))
                        .apply(requestOptions)
                        .into(photo);
            }
            fullName.setText(setColorSpan(resident.getFullName(), filterQuery, colorAccent));
            address.setText(setColorSpan(resident.getFullAddress(), filterQuery, colorAccent));
            work.setText(resident.getWork());
            work.setVisibility(resident.getWork().isEmpty() ? View.GONE : View.VISIBLE);

            itemView.setBackgroundColor(isSelected(getAdapterPosition()) ? selectedBackgroundColor : Color.WHITE);
            divider.setVisibility(getAdapterPosition() == getItemCount() - 1 ? View.GONE : View.VISIBLE);
        }
    }

    private class ResidentFilter extends Filter {
        private final ResidentListAdapter adapter;
        private List<Resident> originalList;
        private List<Resident> filteredList;
        private int tempLength = 0;

        ResidentFilter(ResidentListAdapter adapter, List<Resident> originalList) {
            this.adapter = adapter;
            this.originalList = originalList == null ? new ArrayList<>() : new ArrayList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            final FilterResults results = new FilterResults();

            boolean decreasing = tempLength >= charSequence.length();

            if (charSequence.length() == 0) {
                filteredList.clear();
                results.values = originalList;
                results.count = originalList.size();
                filteredList.addAll(originalList);
            } else {
                if (filteredList.size() > 0 && !decreasing) {
                    List<Resident> temp = new ArrayList<>();
                    for (final Resident item : filteredList) {
                        if (item.getFullName().contains(charSequence) || item.getFullAddress().contains(charSequence)) {
                            temp.add(item);
                        }
                    }
                    filteredList.clear();
                    filteredList = new ArrayList<>(temp);
                    temp.clear();
                } else {
                    filteredList.clear();
                    for (final Resident item : originalList) {
                        if (item.getFullName().contains(charSequence) || item.getFullAddress().contains(charSequence)) {
                            filteredList.add(item);
                        }
                    }
                }
                results.values = filteredList;
                results.count = filteredList.size();
            }

            tempLength = charSequence.length();
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            //noinspection unchecked
            List<Resident> toBeAddedList = (List<Resident>) filterResults.values;

            adapter.filterQuery = String.valueOf(charSequence);
            adapter.searchItems.clear();
            adapter.searchItems.addAll(toBeAddedList);
            adapter.notifyDataSetChanged();
        }
    }
}
