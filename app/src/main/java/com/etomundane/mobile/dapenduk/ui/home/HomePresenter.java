package com.etomundane.mobile.dapenduk.ui.home;

import com.etomundane.mobile.dapenduk.base.BasePresenter;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Date Created: 2018-07-08 02:06:25
 * Author: Eto Mundane
 */
@ActivityScope
public class HomePresenter<V extends HomeMVP.View, M extends HomeMVP.Mediator> extends BasePresenter<V, M> implements HomeMVP.Presenter<V, M> {

    @Inject
    HomePresenter(M mMediator, AppScheduler mScheduler, CompositeDisposable mCompositeDisposable) {
        super(mMediator, mScheduler, mCompositeDisposable);
    }

    @Override
    public void checkLoginStatus() {
        getView().onLoginStatus(getMediator().isAdminLogin());
    }

    @Override
    public void performLogout() {
        getMediator().performLogout();
        getView().onLoginStatus(false);
    }

    @Override
    public void getAllResident() {
        getView().showProgress(true);
        Disposable disposable = getMediator().getAllResident()
                .compose(scheduler.ioSingleScheduler())
                .subscribe(
                        (residents, throwable) -> {
                            if (throwable != null) {
                                getView().showError(throwable.getLocalizedMessage());
                            } else if (residents == null || residents.isEmpty()) {
                                getView().onEmptyResident();
                            } else {
                                getView().onResidentFetched(residents);
                            }
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void createNewResident(Resident resident) {
        getView().showProgress(true);
        Disposable disposable = getMediator().createNewResident(resident)
                .compose(scheduler.ioObservableScheduler())
                .subscribe(uid -> {
                            getView().onNewResidentCreated(uid > 0);
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void deleteResident(long id) {
        getView().showProgress(true);
        Disposable disposable = getMediator().deleteResident(id)
                .compose(scheduler.ioObservableScheduler())
                .subscribe(deleted -> {
                            getView().onNewResidentCreated(deleted);
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void deleteMultipleResident(List<Resident> residents) {
        getView().showProgress(true);
        Disposable disposable = getMediator().deleteMultipleResident(residents)
                .compose(scheduler.ioObservableScheduler())
                .doOnError(e -> getView().showError(e.toString()))
                .subscribe(deleted -> {
                            getView().onResidentDeleted(deleted);
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void updateResident(Resident resident) {
        getView().showProgress(true);
        Disposable disposable = getMediator().updateResident(resident)
                .compose(scheduler.ioObservableScheduler())
                .subscribe(updated -> {
                            getView().onNewResidentCreated(updated);
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }
}
