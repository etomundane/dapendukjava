package com.etomundane.mobile.dapenduk.ui.detail;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.ui.common.AppBarScrollWatcher;
import com.etomundane.mobile.dapenduk.ui.edit.EditActivity;
import com.etomundane.mobile.dapenduk.ui.photo.PhotoViewerActivity;
import com.etomundane.mobile.dapenduk.util.android.AppHelper;
import com.etomundane.mobile.dapenduk.util.android.SystemHelper;
import com.etomundane.mobile.dapenduk.util.android.ViewHelper;
import com.etomundane.mobile.dapenduk.util.common.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;
import timber.log.Timber;

public class DetailActivity extends DaggerAppCompatActivity implements DetailMVP.View {

    public static final String EXTRA_RESIDENT_ID = "extra_id";

    @Inject
    DetailPresenter<DetailMVP.View, DetailMVP.Mediator> presenter;

    @BindView(R.id.activity_detail)
    ViewGroup rootView;

    @BindView(R.id.app_bar)
    AppBarLayout appBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_shadow)
    View toolbarShadow;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.still_photo)
    ImageView stillPhoto;

    @BindView(R.id.moving_photo)
    ImageView movingPhoto;

    @BindView(R.id.progress_bar)
    View progressBar;

    @BindView(R.id.background_photo)
    ImageView profilePhoto;

    @BindView(R.id.full_name)
    TextView tFullName;

    @BindView(R.id.address)
    TextView tAddress;

    @BindView(R.id.gender)
    TextView tGender;

    @BindView(R.id.pob)
    TextView tPob;

    @BindView(R.id.dob)
    TextView tDob;

    @BindView(R.id.work)
    TextView tWork;

    @BindView(R.id.toolbar_edit)
    ImageView vEdit;

    @BindView(R.id.toolbar_shortcut)
    ImageView vShortcut;

    @BindView(R.id.action_edit_2)
    View vEdit2;

    @BindView(R.id.action_delete)
    View vDelete;

    private Unbinder unbinder;
    private long residentId = 0;

    private AppBarScrollWatcher appBarScrollListener = new AppBarScrollWatcher((expanded, collapsed, argbZeroOnExpanded, argbZeroOnCollapsed) -> {
        toolbar.setBackgroundColor(Color.argb(argbZeroOnExpanded, 255, 255, 255));
        toolbarShadow.setBackgroundColor(Color.argb((argbZeroOnCollapsed / 6), 0, 0, 0));

        back.setColorFilter(Color.argb(255, argbZeroOnCollapsed, argbZeroOnCollapsed, argbZeroOnCollapsed), android.graphics.PorterDuff.Mode.SRC_IN);
        vEdit.setColorFilter(Color.argb(255, argbZeroOnCollapsed, argbZeroOnCollapsed, argbZeroOnCollapsed), android.graphics.PorterDuff.Mode.SRC_IN);
        vShortcut.setColorFilter(Color.argb(255, argbZeroOnCollapsed, argbZeroOnCollapsed, argbZeroOnCollapsed), android.graphics.PorterDuff.Mode.SRC_IN);
        toolbarTitle.setTextColor(Color.argb(255, argbZeroOnCollapsed, argbZeroOnCollapsed, argbZeroOnCollapsed));

        stillPhoto.setVisibility(collapsed ? View.VISIBLE : View.INVISIBLE);
        movingPhoto.setVisibility(collapsed ? View.INVISIBLE : View.VISIBLE);
        Timber.i("Collapsed: %s", collapsed);
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        appBar.addOnOffsetChangedListener(appBarScrollListener);
        back.setOnClickListener(v -> onBackPressed());

        presenter.attachView(this);
        presenter.checkLoginStatus();
        residentId = getIntent().getLongExtra(EXTRA_RESIDENT_ID, 0);
        if (residentId > 0) {
            presenter.getResidentById(residentId);
        } else {
            toast(getString(R.string.alert_info_unavailable));
            finish();
        }
    }

    @Override
    protected void onStop() {
        appBar.removeOnOffsetChangedListener(appBarScrollListener);
        super.onStop();
    }

    @Override
    public void onResidentFetched(Resident resident) {
        vShortcut.setEnabled(true);
        vEdit.setEnabled(true);
        vEdit2.setEnabled(true);

        toolbarTitle.setText(resident.getFullName());
        tFullName.setText(resident.getFullName());
        tAddress.setText(resident.getFullAddress());
        tGender.setText(resident.getGender());
        tPob.setText(resident.getPob());
        tDob.setText(SystemHelper.millisToTime(resident.getDob(), Constants.DOB_DATE_FORMAT));
        tWork.setText(resident.getWork());

        View.OnClickListener onPhotoClick = v -> {
            if (resident.getPhoto().isEmpty()) {
                showSimpleSnackBar(getString(R.string.alert_no_photo));
            } else {
                Intent i = new Intent(this, PhotoViewerActivity.class);
                i.putExtra(PhotoViewerActivity.EXTRA_PATH, resident.getPhoto());
                startActivity(i);
            }
        };

        profilePhoto.setOnClickListener(onPhotoClick);
        stillPhoto.setOnClickListener(onPhotoClick);
        movingPhoto.setOnClickListener(onPhotoClick);
        vShortcut.setOnClickListener(v -> {
            try {
                AppHelper.createResidentShortcut(getApplicationContext(), resident);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        int placeholderIcon = resident.getGender().matches(Resident.Gender.FEMALE) ? R.drawable.ic_ph_person_female_80dp : R.drawable.ic_ph_person_male_80dp;
        if (!resident.getPhoto().isEmpty()) {
            try {
                showProgress(true);
                ByteArrayOutputStream photoStream = ViewHelper.blur(this, Uri.parse(resident.getPhoto()), 0.3f, 30);

                RequestOptions requestOptions = new RequestOptions()
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true);
                Glide.with(this)
                        .load(photoStream.toByteArray())
                        .apply(requestOptions)
                        .into(profilePhoto);
                showProgress(false);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        loadCircleImage(Uri.parse(resident.getPhoto()), stillPhoto, placeholderIcon);
        loadCircleImage(Uri.parse(resident.getPhoto()), movingPhoto, placeholderIcon);
    }

    @Override
    public void onResidentDeleted(boolean deleted) {
        if (deleted) {
            toast(getString(R.string.alert_deleted));
            finish();
        } else {
            vEdit.setEnabled(true);
            vDelete.setEnabled(true);
            showSimpleSnackBar(getString(R.string.alert_delete_failed));
        }
    }

    @Override
    public void onLoginStatus(boolean adminLogin) {
        vEdit.setVisibility(adminLogin ? View.VISIBLE : View.GONE);
        vEdit2.setVisibility(adminLogin ? View.VISIBLE : View.GONE);
        vDelete.setVisibility(adminLogin ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(String message) {
        showSimpleSnackBar(message);
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showSimpleSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.toolbar_edit)
    void onClickEdit() {
        vEdit.setEnabled(false);
        vEdit2.setEnabled(false);
        Intent i = new Intent(this, EditActivity.class);
        i.putExtra(EditActivity.EXTRA_RESIDENT_ID, residentId);
        startActivity(i);
    }

    @OnClick(R.id.action_edit_2)
    void onClickEdit2() {
        onClickEdit();
    }

    @OnClick(R.id.action_delete)
    void onClickDelete() {
        ViewHelper.showConfirmDialog(this, getString(R.string.title_delete), getString(R.string.alert_delete_resident), getString(R.string.title_delete), getString(R.string.title_cancel), (dialog, which) ->
                presenter.onClickDelete(residentId)
        );
    }

    private void loadCircleImage(Uri uri, ImageView target, int placeHolder) {
        RequestOptions requestOptions = new RequestOptions()
                .dontAnimate()
                .circleCrop()
                .placeholder(placeHolder)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        Glide.with(this)
                .load(uri)
                .apply(requestOptions)
                .into(target);
    }

}
