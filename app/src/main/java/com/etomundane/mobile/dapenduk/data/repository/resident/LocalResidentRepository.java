package com.etomundane.mobile.dapenduk.data.repository.resident;

import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.data.local.database.resident.ResidentDao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-06 01:39:15
 * Author: Eto Mundane
 */
@Singleton
public class LocalResidentRepository implements ResidentRepository {

    private ResidentDao residentDao;

    @Inject
    public LocalResidentRepository(ResidentDao residentDao) {
        this.residentDao = residentDao;
    }

    @Override
    public Observable<Long> persistResident(final Resident resident) {
        return Observable.fromCallable(() -> residentDao.insert(resident));
    }

    @Override
    public Observable<Boolean> deleteResident(long id) {
        return Observable.fromCallable(() -> residentDao.deleteResident(id) > 0);
    }

    @Override
    public Observable<Boolean> deleteResidents(List<Resident> residents) {
        return Observable.fromCallable(() -> residentDao.delete(residents) > 0);
    }

    @Override
    public Single<List<Resident>> getAllResident() {
        return Single.fromCallable(() -> residentDao.getAllResident());
    }

    @Override
    public Single<Resident> getResident(long id) {
        return Single.fromCallable(() -> residentDao.getResident(id));
    }

    @Override
    public Observable<Boolean> updateResident(Resident resident) {
        return Observable.fromCallable(() -> residentDao.update(resident) > 0);
    }
}
