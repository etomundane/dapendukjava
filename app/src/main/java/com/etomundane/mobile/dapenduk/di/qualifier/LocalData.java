package com.etomundane.mobile.dapenduk.di.qualifier;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Date Created: 2018-07-05 23:56:58
 * Author: Eto Mundane
 */
@Qualifier
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface LocalData {
}
