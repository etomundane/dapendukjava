package com.etomundane.mobile.dapenduk.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.etomundane.mobile.dapenduk.data.local.database.AppDatabase;
import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;
import com.etomundane.mobile.dapenduk.data.local.preference.PreferenceHelper;
import com.etomundane.mobile.dapenduk.data.repository.resident.LocalResidentRepository;
import com.etomundane.mobile.dapenduk.data.repository.resident.ResidentRepository;
import com.etomundane.mobile.dapenduk.data.repository.user.LocalUserRepository;
import com.etomundane.mobile.dapenduk.data.repository.user.UserRepository;
import com.etomundane.mobile.dapenduk.di.qualifier.ApplicationScope;
import com.etomundane.mobile.dapenduk.di.qualifier.DatabaseInfo;
import com.etomundane.mobile.dapenduk.di.qualifier.LocalData;
import com.etomundane.mobile.dapenduk.di.qualifier.PreferenceInfo;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;
import com.etomundane.mobile.dapenduk.util.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Date Created: 2018-07-05 22:39:46
 * Author: Eto Mundane
 */
@Module
public abstract class AppModule {
    @ApplicationScope
    @Binds
    abstract Context bindContext(Application application);

    @Provides
    static AppScheduler provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    static CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @DatabaseInfo
    static String provideDatabaseName() {
        return AppDatabase.DB_NAME;
    }

    @Provides
    @PreferenceInfo
    static String providePreferenceName() {
        return PreferenceHelper.PREF_NAME;
    }

    @LocalData
    @Binds
    abstract AppPreference providePreferencesHelper(PreferenceHelper preferenceHelper);

    @Provides
    @Singleton
    static AppDatabase provideAppDatabase(@ApplicationScope Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, AppDatabase.DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    static UserRepository provideLocalUserRepository(AppDatabase appDatabase) {
        return new LocalUserRepository(appDatabase.userDao());
    }

    @Provides
    @Singleton
    static ResidentRepository provideLocalResidentRepository(AppDatabase appDatabase) {
        return new LocalResidentRepository(appDatabase.residentDao());
    }


}
