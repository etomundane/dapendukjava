package com.etomundane.mobile.dapenduk.ui.login;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * Date Created: 2018-07-08 18:29:56
 * Author: Eto Mundane
 */
@Module
public abstract class LoginModule {
    @ActivityScope
    @Binds
    abstract LoginMVP.Mediator loginMediator(@ActivityScope LoginMediator mediator);

    @ActivityScope
    @Binds
    abstract LoginMVP.Presenter loginPresenter(@ActivityScope LoginPresenter<LoginMVP.View, LoginMVP.Mediator> presenter);

}
