package com.etomundane.mobile.dapenduk.data.local.database.user;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.etomundane.mobile.dapenduk.base.BaseDao;

import java.util.List;

/**
 * Date Created: 2018-07-06 00:55:06
 * Author: Eto Mundane
 */
@Dao
public interface UserDao extends BaseDao<User> {
    @Query("select * from " + User.TABLE_NAME + " where " + User.C_ROLE + "=" + User.Role.ADMIN)
    List<User> getAllAdmin();

    @Query("select * from " + User.TABLE_NAME + " where " + User.C_USERNAME + "=:username and " + User.C_PASSWORD + "=:password")
    User getUser(String username, String password);

    @Query("select * from  " + User.TABLE_NAME + " where " + User.C_ID + "= :uid")
    User getUser(long uid);

    @Query("delete from  " + User.TABLE_NAME)
    void clearTable();
}
