package com.etomundane.mobile.dapenduk.ui.edit;

import com.etomundane.mobile.dapenduk.base.BaseMediator;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;
import com.etomundane.mobile.dapenduk.data.repository.resident.ResidentRepository;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.di.qualifier.LocalData;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;


/**
 * Date Created: 2018-07-09 00:14:59
 * Author: Eto Mundane
 */
@ActivityScope
public class EditMediator extends BaseMediator implements EditMVP.Mediator {
    private ResidentRepository repository;

    @Inject
    EditMediator(@LocalData AppPreference pref, @Singleton ResidentRepository residentRepository) {
        super(pref);
        this.repository = residentRepository;
    }

    @Override
    public Single<Resident> getResident(long id) {
        return repository.getResident(id);
    }

    @Override
    public Observable<Boolean> obtainEdit(Resident resident) {
        return repository.updateResident(resident);
    }
}
