package com.etomundane.mobile.dapenduk.ui.detail;

import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * Date Created: 2018-07-08 23:19:23
 * Author: Eto Mundane
 */
@Module
public abstract class DetailModule {
    @ActivityScope
    @Binds
    abstract DetailMVP.Mediator detailMediator(@ActivityScope DetailMediator mediator);

    @ActivityScope
    @Binds
    abstract DetailMVP.Presenter detailPresenter(@ActivityScope DetailPresenter<DetailMVP.View, DetailMVP.Mediator> presenter);

}
