package com.etomundane.mobile.dapenduk.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.etomundane.mobile.dapenduk.R;
import com.etomundane.mobile.dapenduk.ui.home.HomeActivity;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class SplashActivity extends DaggerAppCompatActivity implements SplashMVP.View {
    @Inject
    SplashPresenter<SplashMVP.View, SplashMVP.Mediator> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar_FullScreen);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter.attachView(this);
        presenter.initDefaultAdmin();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void goToHome() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    @Override
    public void showProgress(boolean show) {

    }

    @Override
    public void showError(String message) {
        toast(message);
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
