package com.etomundane.mobile.dapenduk.ui.detail;

import com.etomundane.mobile.dapenduk.base.BaseMVP;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Date Created: 2018-07-08 22:34:55
 * Author: Eto Mundane
 */
public interface DetailMVP {
    interface Mediator extends BaseMVP.Mediator {
        Single<Resident> getResident(long id);

        Observable<Boolean> deleteResident(long id);
    }

    interface View extends BaseMVP.View {
        void onResidentFetched(Resident resident);

        void onResidentDeleted(boolean deleted);

        void onLoginStatus(boolean adminLogin);
    }

    interface Presenter<V extends DetailMVP.View, M extends DetailMVP.Mediator> extends BaseMVP.Presenter<V, M> {
        void checkLoginStatus();

        void getResidentById(long id);

        void onClickDelete(long id);
    }
}
