package com.etomundane.mobile.dapenduk.ui.create;

import com.etomundane.mobile.dapenduk.base.BaseMediator;
import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;
import com.etomundane.mobile.dapenduk.data.repository.resident.ResidentRepository;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.di.qualifier.LocalData;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


/**
 * Date Created: 2018-07-08 18:51:59
 * Author: Eto Mundane
 */
@ActivityScope
public class CreateMediator extends BaseMediator implements CreateMVP.Mediator {

    private ResidentRepository repository;

    @Inject
    public CreateMediator(@LocalData AppPreference pref, @Singleton ResidentRepository residentRepository) {
        super(pref);
        this.repository = residentRepository;
    }

    @Override
    public void setUnsavedResident(Resident resident) {
        pref.setUnsavedResident(resident);
    }

    @Override
    public Resident getUnsavedResident() {
        return pref.getUnsavedResident();
    }

    @Override
    public void clearUnsavedResident() {
        pref.clearUnsavedResident();
    }

    @Override
    public Observable<Long> obtainCreate(Resident resident) {
        return repository.persistResident(resident);
    }
}
