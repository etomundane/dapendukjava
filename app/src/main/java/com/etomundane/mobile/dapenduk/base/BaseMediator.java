package com.etomundane.mobile.dapenduk.base;

import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;

/**
 * Date Created: 2018-07-05 23:13:50
 * Author: Eto Mundane
 */
public abstract class BaseMediator implements BaseMVP.Mediator {

    protected AppPreference pref;

    public BaseMediator(AppPreference pref) {
        this.pref = pref;
    }

    @Override
    public boolean isAdminLogin() {
        return pref.isLogin();
    }

    @Override
    public void performLogout() {
        pref.setLogin(false);
        if (!pref.isPasswordRemembered()) {
            pref.clearRememberedPassword();
        }
    }
}
