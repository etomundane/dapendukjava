package com.etomundane.mobile.dapenduk.ui.splash;

import com.etomundane.mobile.dapenduk.base.BaseMVP;

import io.reactivex.Observable;

/**
 * Date Created: 2018-07-06 23:59:03
 * Author: Eto Mundane
 */
public interface SplashMVP {
    interface Mediator extends BaseMVP.Mediator {
        Observable<Boolean> isAdminEmpty();
        Observable<Long> insertDefaultAdmin();
    }

    interface View extends  BaseMVP.View {
        void goToHome();
    }

    interface Presenter<V extends View, M extends Mediator> extends BaseMVP.Presenter<V, M> {
        void initDefaultAdmin();
    }
}
