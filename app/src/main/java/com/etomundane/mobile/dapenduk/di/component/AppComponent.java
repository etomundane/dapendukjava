package com.etomundane.mobile.dapenduk.di.component;

import android.app.Application;

import com.etomundane.mobile.dapenduk.Dapenduk;
import com.etomundane.mobile.dapenduk.di.module.ActivityBuilder;
import com.etomundane.mobile.dapenduk.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Date Created: 2018-07-05 21:43:09
 * Author: Eto Mundane
 */

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class
})
public interface AppComponent extends AndroidInjector<Dapenduk> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
