package com.etomundane.mobile.dapenduk.data.local.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;
import com.etomundane.mobile.dapenduk.data.local.database.resident.ResidentDao;
import com.etomundane.mobile.dapenduk.data.local.database.user.User;
import com.etomundane.mobile.dapenduk.data.local.database.user.UserDao;


/**
 * Date Created: 2018-07-06 00:06:47
 * Author: Eto Mundane
 */
@Database(entities = {Resident.class, User.class}, version = AppDatabase.DB_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DB_NAME = "pq5vPWRNckQfLrQkfXaxrwwwBBXt97Rd";
    static final int DB_VERSION = 1;

    public abstract ResidentDao residentDao();

    public abstract UserDao userDao();
}
