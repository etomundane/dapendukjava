package com.etomundane.mobile.dapenduk.ui.detail;

import com.etomundane.mobile.dapenduk.base.BasePresenter;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Date Created: 2018-07-08 22:44:07
 * Author: Eto Mundane
 */
@ActivityScope
public class DetailPresenter<V extends DetailMVP.View, M extends DetailMVP.Mediator> extends BasePresenter<V, M> implements DetailMVP.Presenter<V, M> {

    @Inject
    DetailPresenter(M mMediator, AppScheduler scheduler, CompositeDisposable compositeDisposable) {
        super(mMediator, scheduler, compositeDisposable);
    }


    @Override
    public void checkLoginStatus() {
        getView().onLoginStatus(getMediator().isAdminLogin());
    }

    @Override
    public void getResidentById(long id) {
        getView().showProgress(true);
        Disposable disposable = getMediator().getResident(id)
                .compose(scheduler.ioSingleScheduler())
                .subscribe(
                        (resident, throwable) -> {
                            if (throwable != null) {
                                getView().showError(throwable.getLocalizedMessage());
                            } else if (resident != null) {
                                getView().onResidentFetched(resident);
                            }
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void onClickDelete(long id) {
        getView().showProgress(true);
        Disposable disposable = getMediator().deleteResident(id)
                .compose(scheduler.ioObservableScheduler())
                .subscribe(deleted -> {
                            getView().onResidentDeleted(deleted);
                            getView().showProgress(false);
                        }
                );
        compositeDisposable.add(disposable);
    }
}
