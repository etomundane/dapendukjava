package com.etomundane.mobile.dapenduk.ui.login;


import android.support.v4.util.Pair;

import com.etomundane.mobile.dapenduk.base.BasePresenter;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.util.rx.AppScheduler;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Date Created: 2018-07-08 17:40:15
 * Author: Eto Mundane
 */
@ActivityScope
public class LoginPresenter<V extends LoginMVP.View, M extends LoginMVP.Mediator> extends BasePresenter<V, M> implements LoginMVP.Presenter<V, M> {

    @Inject
    LoginPresenter(M mMediator, AppScheduler mScheduler, CompositeDisposable mCompositeDisposable) {
        super(mMediator, mScheduler, mCompositeDisposable);
    }

    @Override
    public void isRememberPassword() {
        getView().onRememberPassword(getMediator().isRememberPassword());
    }

    @Override
    public void getRememberedPassword() {
        Pair<String, String> pair = getMediator().getRememberedPassword();
        getView().onPasswordRemembered(pair.first, pair.second);
    }

    @Override
    public void onSubmitLogin(String username, String password, boolean rememberPassword) {
        getView().showProgress(true);
        Disposable disposable = getMediator().obtainLogin(username, password)
                .compose(scheduler.ioSingleScheduler())
                .subscribe((user, throwable) -> {
                    getView().showProgress(false);
                    if (user != null) {
                        getMediator().updateCurrentAdmin(user, rememberPassword);
                        getView().onLoginSuccess();
                    } else if (throwable != null) {
                        getView().showError(throwable.getLocalizedMessage());
                    } else {
                        getView().onUserNotExisted();
                    }
                });

        compositeDisposable.add(disposable);

    }
}
