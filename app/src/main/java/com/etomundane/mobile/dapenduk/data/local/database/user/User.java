package com.etomundane.mobile.dapenduk.data.local.database.user;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Date Created: 2018-07-06 00:41:23
 * Author: Eto Mundane
 */
@Entity(tableName = User.TABLE_NAME)
public final class User {
    public static final String TABLE_NAME = "user";
    public static final String C_ID = "id";
    public static final String C_USERNAME = "username";
    public static final String C_PASSWORD = "password";
    public static final String C_ROLE = "role";
    private static final String C_FULL_NAME = "full_name";

    public interface Role {
        int ADMIN = 1;
        int PUBLIC = 2;
    }

    @ColumnInfo(name = User.C_ID)
    @PrimaryKey(autoGenerate = true)
    private
    long uid;

    @ColumnInfo(name = User.C_USERNAME)
    String username;

    @ColumnInfo(name = User.C_PASSWORD)
    String password;

    @ColumnInfo(name = User.C_ROLE)
    private
    int userRole;

    @ColumnInfo(name = User.C_FULL_NAME)
    private
    String fullName;

    public User(String username, String password, int userRole, String fullName) {
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.fullName = fullName;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getUid() {
        return uid;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getUserRole() {
        return userRole;
    }

    public String getFullName() {
        return fullName;
    }
}
