package com.etomundane.mobile.dapenduk.data.local.preference;

import com.etomundane.mobile.dapenduk.data.local.database.resident.Resident;

/**
 * Date Created: 2018-07-06 00:09:28
 * Author: Eto Mundane
 */
public interface AppPreference {
    void setLogin(boolean isLogin);

    boolean isLogin();

    void setUserId(long uid);

    long getCurrentUserId();

    void setRememberPassword(boolean remember);

    boolean isPasswordRemembered();

    void saveRememberedPassword(String username, String password);

    void clearRememberedPassword();

    String getRememberedUsername();

    String getRememberedPassword();

    void setUnsavedResident(Resident resident);

    Resident getUnsavedResident();

    void clearUnsavedResident();
}
