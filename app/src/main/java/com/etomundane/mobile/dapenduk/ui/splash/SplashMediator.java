package com.etomundane.mobile.dapenduk.ui.splash;

import com.etomundane.mobile.dapenduk.base.BaseMediator;
import com.etomundane.mobile.dapenduk.data.local.database.user.User;
import com.etomundane.mobile.dapenduk.data.local.preference.AppPreference;
import com.etomundane.mobile.dapenduk.data.repository.user.UserRepository;
import com.etomundane.mobile.dapenduk.di.qualifier.ActivityScope;
import com.etomundane.mobile.dapenduk.di.qualifier.LocalData;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Date Created: 2018-07-07 10:51:27
 * Author: Eto Mundane
 */
@ActivityScope
public class SplashMediator extends BaseMediator implements SplashMVP.Mediator {

    private final UserRepository userRepo;

    @Inject
    SplashMediator(@LocalData AppPreference pref, @Singleton UserRepository userRepository) {
        super(pref);
        this.userRepo = userRepository;
    }

    @Override
    public Observable<Boolean> isAdminEmpty() {
        return userRepo.isAdminEmpty();
    }


    @Override
    public Observable<Long> insertDefaultAdmin() {
        User admin = new User("admin", "666666", User.Role.ADMIN, "Super Da");
        return userRepo.insertAdminUser(admin);
    }
}
